from django.shortcuts import render, get_object_or_404
from django.forms import ModelForm, Textarea, SelectDateWidget
from LesTaches.models import Task
from django import forms
from django.template import RequestContext
from django.http import HttpResponse
from django.http import HttpResponseRedirect
import datetime



def home(request, name=None):
	if name is not None:
		return HttpResponse('Bonjour à tous '+name)
	else:
		return HttpResponse('Bonjour à tous')


# ANCIEN TASK_LISTING SANS LE TEMPLATE
# def task_listing(request):
# 	from django.template import Template, Context
# 	objets = Task.objects.all().order_by('due_date')
# 	template = Template("{% for elem in objets %} {{elem}} <br/> {%endfor%}")
# 	print(str(template))
# 	context = Context({'objets':objets})
# 	print(str(template.render(context)))
# 	return HttpResponse(template.render(context))

#NOUVEAU TASK_LISTING AVEC TEMPLATE
def task_listing(request):
	objets = Task.objects.all().order_by('due_date')
	return render(request, 'LesTaches/list.html',{'objets':objets})

def task_listing2(request):
	objets = Task.objects.all().order_by('-due_date')
	#print(objets)
	# - : inverse l'ordre
	return render(request,'LesTaches/list.html', {'taches':objets})

class TaskForm(ModelForm):
	def __init__(self, *args, **kwargs):
		super(TaskForm, self).__init__(*args, **kwargs)
		self.fields['name'].label = "Nom"
		self.fields['description'].label = "description"
		self.fields['schedule_date'].label = "schedule_date"
		self.fields['due_date'].label = "due_date"
		self.fields['closed'].label = "closed"
	class Meta:
		model = Task
		fields = ('name','description','schedule_date','due_date','closed',)
		widgets = {'message': Textarea(attrs={'cols':60, 'rows':10}),'schedule_date':SelectDateWidget(),'due_date':SelectDateWidget()}


def task1(request):
	form = TaskForm()
	con = {'form': form}
	if len(request.POST) > 0:
		form = TaskForm(request.POST)
		con = {'form': form}
		if form.is_valid():
			con = form.save(commit=False)
			con.save()
			return HttpResponseRedirect("/LesTaches/listing")
	return render(request, 'LesTaches/ajoutTache.html', con)

def task2(request):
	form = TaskForm()
	con = {'form': form}
	if request.method == 'GET':
		return render(request,'LesTaches/ajoutTache.html',con,context_instance=RequestContext(request))
	else:
		form = TaskForm(request.POST)
		con = {'form': form}
		if form.is_valid():
			form.save()
			return HttpResponseRedirect("/LesTaches/listing")
	return render(request,'LesTaches/ajoutTache.html',con)

def delete(request,id):
    Task.objects.get(id=id).delete()
    return HttpResponseRedirect("/LesTaches/listing")

def maj(request, id):
	task = get_object_or_404(Task, id=id)
	form = TaskForm(instance=task)
	con = {'form': form}
	if len(request.POST) > 0:
		form = TaskForm(request.POST, instance=task)
		con = {'form': form}
		if form.is_valid():
			con = form.save(commit=False)
			con.save()
			return HttpResponseRedirect("/LesTaches/listing")
	return render(request, 'LesTaches/ajoutTache.html', con)

def maj2(request, id):
	con = {}
	task = get_object_or_404(Task, id=id)
	if request.method == 'GET':
		return render(request,'LesTaches/ajoutTache.html',con)
	else:
		form = TaskForm(request.POST, instance=task)
		con = {'form': form}
		if form.is_valid():
			form.save()
			return HttpResponseRedirect("/LesTaches/listing")
	return render(request,'LesTaches/ajoutTache.html',con)



def success(request):
	return HttpResponse('<p>Success</p>')
