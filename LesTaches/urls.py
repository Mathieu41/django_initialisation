"""GestionTaches URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^home/(\w+)?$', views.home, name='home'),
    url(r'^listing$', views.task_listing, name="listing"),
    url(r'^delete/(?P<id>\w+)', views.delete, name="delete"),
    url(r'^maj/(?P<id>\w+)', views.maj, name="maj"),
    url(r'^ajout$', views.task1, name="ajout"),
    url(r'^ajout$', views.task2, name="ajout2"),
]
