# A mettre dans le fichier tests.py de l'app Django
from django.core.urlresolvers import resolve
from django.test import TestCase
from LesTaches.views import *
from selenium import webdriver
import time

class PageTest(TestCase):
    '''Test unitaire de la page accueil sur la racine du projet'''
    def test_root_url_resolves_list_taches(self):
        found = resolve('/LesTaches/listing')
        self.assertEqual(found.func, task_listing)

    def test_root_url_resolves_ajout_tache(self):
        found = resolve('/LesTaches/ajout')
        self.assertEqual(found.func, task1)

    def test_selenium1(self):
        browser = webdriver.Firefox()
        browser.get('http://localhost:8000/LesTaches/listing')
        assert 'Liste des taches' in browser.title
        browser.close()

    def test_selenium2(self):
        browser.get('http://localhost:8000/LesTaches/ajout')
        assert "Ajout d'une tache" in browser.title
        browser.close()

class AppTest(TestCase):

    def test_ajout(self):
        baseurl = "http://localhost:8000/LesTaches/ajout"
        nom = "test1"
        description = "description1"
        closed=True


        xpaths = { 'champNom' : "//input[@id='id_name']",
                   'champDescription' : "//textarea[@id='id_description']",
                   'champClosed' : "//input[@id='id_closed']",
                   'submitButton' :   "//input[@type='submit']"
                 }

        browser = webdriver.Firefox(executable_path='./geckodriver')
        browser.get(baseurl)
        browser.maximize_window()

        #Clear Username TextBox if already allowed "Remember Me"
        browser.find_element_by_xpath(xpaths['champNom']).clear()

        #Write Username in Username TextBox
        browser.find_element_by_xpath(xpaths['champNom']).send_keys(nom)

        #Clear Password TextBox if already allowed "Remember Me"
        browser.find_element_by_xpath(xpaths['champDescription']).clear()

        # Write Password in password TextBox
        browser.find_element_by_xpath(xpaths['champDescription']).send_keys(description)

        #Clear Password TextBox if already allowed "Remember Me"
        browser.find_element_by_xpath(xpaths['champClosed']).click()


        #Click Login button
        browser.find_element_by_xpath(xpaths['submitButton']).click()

        # print(browser.page_source)

        assert 'Liste des taches' in browser.title
        browser.close()
