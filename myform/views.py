from django.shortcuts import render

# Create your views here.

from django.forms import ModelForm, Textarea
from myform.models import Contact
from django import forms
from django.template import RequestContext
from django.http import HttpResponse
from django.http import HttpResponseRedirect

# class ContactForm(ModelForm):
# 	class Meta:
# 		model = Contact
# 		fields = ('name', 'firstname', 'email', 'message')

# Ancien contact
# def contact(request):
# 	contact_form = ContactForm()
# 	return render(request,'myform/contact.html',{
# 		'contact_form' : contact_form
# 	})


# class ContactForm2(forms.Form):
# 	name = forms.CharField(max_length=200)
# 	firstname = forms.CharField(max_length=200)
# 	email = forms.EmailField(max_length=200)
# 	message = forms.CharField(max_length=1000)

# def contact(request):
# 	contact_form = ContactForm()
# 	contact_form2 = ContactForm2()
# 	return render(request,'myform/contact.html',{'contact_form' : contact_form, 'contact_form2' : contact_form2})

# class ContactForm(ModelForm):

# 	class Meta:
# 		model = Contact
# 		fields = ('name','firstname','email','message')
# 		widgets = {
# 			'message' : Textarea(attrs={'cols' : 60, 'rows' : 10}),
# 		}

# class ContactForm2(forms.Form):
# 	name = forms.CharField(max_length=200, initial = "Votre nom", label = "nom")
# 	firstname = forms.CharField(max_length=200, initial = "Votre prenom", label = "nom")
# 	email = forms.EmailField(max_length=200, label = "mel")
# 	message = forms.CharField(widget = forms.Textarea(attrs = {'cols' : 60, 'rows' : 10}))

# def contact(request):
# 	contact_form = ContactForm()

# 	contact_form2 = ContactForm2()
# 	return render(request,'myform/contact.html',{'contact_form' : contact_form, 'contact_form2' : contact_form2})

class ContactForm(ModelForm):
	def __init__(self, *args, **kwargs):
		super(ContactForm, self).__init__(*args, **kwargs)
		self.fields['name'].label = "Nom"
		self.fields['firstname'].label = "Prenom"
		self.fields['email'].label = "Mel"
	class Meta:
		model = Contact
		fields = ('name','firstname','email','message')
		widgets = {'message': Textarea(attrs={'cols':60, 'rows':10}),}

def contact(request):
	form = ContactForm()
	con = {'form': form}
	if len(request.POST) > 0:
		form = ContactForm(request.POST)
		con = {'form': form}
		if form.is_valid():
			cont = form.save(commit=False)
			cont.save()
			return HttpResponseRedirect("/Contacts/success")
	return render(request, 'myform/contact.html', con)

def contact2(request):
	form = ContactForm()
	con = {'form': form}
	if request.method == 'GET':
		return render(request,'myform/contact.html',con,context_instance=RequestContext(request))
	else:
		form = ContactForm(request.POST)
		con = {'form': form}
		if form.is_valid():
			form.save()
			return HttpResponseRedirect("/Contacts/success")
	return render(request,'myform/contact.html',con)

def success(request):
	return HttpResponse('<p>Success </p>')
